BEGIN{
    RS="=========="
    FS="\r\n"
}

{
    if($2){
	if($3 ~ /Highlight/ ){
	    content=sprintf("1. =%s=",$5);
	}else{
	    content=sprintf(" %s",$5);
	}
	if(!book[$2]){
	    book[$2]=sprintf("%s",content);
	}else{
	    book[$2]=sprintf("%s\n\n%s",book[$2],content)
	}
    }
}
END{
    for( i in book){
	print "* " i 
	print book[i]
   }
}
